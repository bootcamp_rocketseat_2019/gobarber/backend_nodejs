## Projeto GoBarber

**Esse foi um dos projetos criados durante o bootcamp. O intuito é criar uma
API para que os usuários cadastrados possam agendar ou cancelar agendamento
com um prestador de serviços de beleza.**


- Neste projeto foi criada um api utilizando expressjs para troca de informações
com o frontend.

- A API se comunica com um banco postgres sql através do sequelize. Nesse banco 
de dados serão salvas todas as informações gerais de cadastro.

- Utiliza-se o banco de dados noSql mongo para armazenamento de notificações
para o prestador de serviços. A comunicação é feita através do mongoose.

- O banco de dados noSql Redis é utlizado para gerenciar o envio de e-mails.
Cria-se filas para o envio de email, através do uso do bee-queue.

- Para o envio de email foi utilzado o nodemailer. Nessa versão ainda estou
usando o mailstrap.io como servidor de e-mail.

- Para o gerenciamento de exceptions utilizamos o Sentry, uma excelente 
ferramenta para detecção de erros de desenvolvimento e server side.

- Projeto configurado com o eslint utilizando com o padrão publico a estilização
da AIRBNB. Utiliza-se também o prettier para que o código fique mais agradável
e com uma maior qualidade.

- Trabalhamos também com a criação de autenticação do usuário utilizando bcrypt 
e JWT.

- É abordado o padrão de middlewares para realizar filtros nas rotas rest. 
Por exemplo, um usuário só pode acessar determinada rota da aplicação se 
estiver com o token JWT válido.

- Para upload de imagens configuramos o multer.